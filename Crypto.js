
const puppeteer = require('puppeteer');

(async () => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.goto('https://coinmarketcap.com', {waitUntil: 'networkidle0'})
    
    let cryptoData = await page.evaluate(async () => {
        let cryptos = [];
        // old coinmarket  cmc-table__table-wrapper-outer
        let tableRows = document.querySelectorAll('div.rc-table-content table tbody > tr');
        tableRows.forEach(row => {
            const cells = row.getElementsByTagName('td');
            if (!cells[0].innerText) {
                return;
            }
            cryptos.push({
                rank: cells[0].innerText,
                // name: cells[1].querySelector('a').innerText,
                // marketCap: cells[2].innerText,
                // price: cells[3].querySelector('a').innerText,
                // volume: cells[4].querySelector('a').innerText,
                // circulatingSupply: cells[5].innerText,
                // priceChange24h: cells[6].innerText
            });
        });
        return {
            results: cryptos.length,
            data: cryptos,
        };
    });
    console.log("RESULT:", cryptoData);
    await browser.close();
})();


