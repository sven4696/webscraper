// use express to set up api endpoint and return this
// app.get('some endpoitn', async(req, res) => { return res.json(cryptodata)})

const express = require('express');
const puppeteer = require('puppeteer');
const app = express();
const cors = require('cors')

// configure this properly
var corsOptions = {
    origin: 'http://xiso.io',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
  }

app.use(express.json());
app.use(cors(corsOptions));
const { PORT = 3000 } = process.env;

const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');

const adapter = new FileSync('db.json')
const db = low(adapter);


db.defaults({ coins: []}).write();


const scrapeCoins = async () => {

    try {
        const browser = await puppeteer.launch({args: ['--no-sandbox']});
        const page = await browser.newPage();
        await page.goto('https://coinmarketcap.com', {waitUntil: 'networkidle0'})
        
        let cryptoData = await page.evaluate(async () => {
            let cryptos = [];
            let tableRows = document.querySelectorAll('div.cmc-table__table-wrapper-outer table tbody > tr');
            tableRows.forEach(row => {
                const cells = row.getElementsByTagName('td');
                if (!cells[0].innerText) {
                    return;
                }
                cryptos.push({
                    name: cells[1].querySelector('a').innerText,
                    price: cells[3].querySelector('a').innerText,
                    priceChange24h: cells[6].innerText
                });
            });
            return cryptos
        });
    
        await browser.close();
        db.setState({coins: cryptoData})
    } catch (error) {
        console.log(error);
    }
   
}

setInterval(() => {
    scrapeCoins();
}, 3600000); 

scrapeCoins();

app.get('/coins', async(req, res) => {


    const coins = db.get('coins')

    return res.json(coins);
})


app.listen(PORT, ()=> console.log(`Running server...`));