const rp = require("request-promise");
const util = require("util");
const express = require("express");
const app = express();
const cors = require("cors");

var CryptoData = [];
const COINS = [
  "bitcoin",
  "xrp",
  "ethereum",
  "vechain",
  "chainlink",
  "cardano",
  "iota",
  "polkadot",
  "hedera hashgraph",
  "binance coin",
  "uniswap",
  "stellar",
  "monero",
  "eos",
  "tezos",
];
var corsOptions = {
  origin: "http://xiso.io",
  optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
};

app.use(express.json());
app.use(cors(corsOptions));
const { PORT = 3000 } = process.env;

const low = require("lowdb");
const FileSync = require("lowdb/adapters/FileSync");

const adapter = new FileSync("db.json");
const db = low(adapter);

db.defaults({ coins: [] }).write();
const requestOptions = {
  method: "GET",
  uri: "https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest",
  qs: {
    start: "1",
    limit: "100",
    convert: "USD",
  },
  headers: {
    "X-CMC_PRO_API_KEY": "96ed4124-d315-4da6-88ac-b6062240d147",
  },
  json: true,
  gzip: true,
};
async function getDigitalAssets() {
  rp(requestOptions)
    .then((response) => response)
    .then((data) => {
      CryptoData.splice(0, CryptoData.length);

      return data.data;
    })
    .then((data) => {
      data.filter((coin) => {
        if (COINS.includes(coin.name.toLowerCase())) {
          CryptoData.push(coin);
        }
      });
      console.log("cyrptoData:", CryptoData);
      console.log("cryptoDataLength: ", CryptoData.length);
      return data;
    })
    .then((result) => {
      db.setState({ coins: result }).write();
      return result;
    })
    .catch((err) => {
      console.log("API call error:", err.message);
    });
}

setInterval( async () => {
  await getDigitalAssets();
  console.log('hello its working')
}, 300000);

app.get("/coins", async (req, res) => {
  const coins = db.get('coins')
  //await getDigitalAssets();

  return res.json(coins);
});

app.listen(PORT, () => console.log(`Running server...`));
